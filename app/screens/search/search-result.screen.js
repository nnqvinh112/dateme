import React, { Component } from 'react';
import { ProfileService } from '../../services/profile.service';
import { PREFERENCE_OPTIONS } from '../../constants/preference.constant';
import { Container, Content, Text, Spinner } from 'native-base';
import ProfileCardComponent from '../../components/profile-card/profile-card.component';
import { StyleSheet, View } from 'react-native';
import { Preference } from '../../models/preference';
import * as _ from 'lodash';
import { STYLE } from '../../constants/style.constant';
import Dimensions from 'Dimensions';

const vh = Dimensions.get('window').height;
const styles = StyleSheet.create({
  content: {
    paddingTop: '2.5%',
    marginLeft: 'auto',
    marginRight: 'auto',
  },

  centerView: {
    height: vh,
    justifyContent: 'center',
    alignItems: 'center',
  }
});

export default class SearchResultScreen extends Component {
  state = {
    profiles: null,
    preference: null,
    isLoading: false,
  }

  constructor(props) {
    super(props);
  }

  componentDidMount() {
    this.loadData();
  }

  componentDidUpdate() {
    this.loadData();
  }

  /**
   * @returns {Preference}
   */
  loadParams() {
    const { navigation } = this.props;
    const preference = navigation.getParam('preference');
    return preference;
  }

  loadData() {
    const preference = this.loadParams();
    if (!_.isEqual(preference, this.state.preference)) {
      this.setState({ preference: _.cloneDeep(preference), isLoading: true });
      ProfileService.getAll({includeSelf: false, ...preference}).then(res => {
        this.setState({ profiles: res, isLoading: false })
        this.forceUpdate();
      });
    }
  }

  render() {
    const { profiles, isLoading } = this.state;
    if (isLoading) {
      return (
        <Container>
          <Content>
            <View style={styles.centerView}>
              <Spinner color={STYLE.COLOR.PRIMARY} />
            </View>
          </Content>
        </Container>
      );
    }
    if (!profiles || !profiles.length) {
      return (
        <Container>
          <Content>
            <View style={styles.centerView}>
              <Text>No result matches your preference</Text>
            </View>
          </Content>
        </Container>
      );
    }
    return (
      <Container>
        <Content style={styles.content}>
          {
            this.state.profiles.map((i) => {
              return (
                <ProfileCardComponent
                  key={i.Id}
                  profile={i}
                  navigation={this.props.navigation}
                />
              )
            })
          }
        </Content>
      </Container>
    )
  }
}

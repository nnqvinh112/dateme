import ChatScreen from './app/screens/chat/chat.screen';
import LoginScreen from './app/screens/login/login.screen';
import ProfileScreen from './app/screens/profile/profile.screen';
import React from "react";
import { createStackNavigator, createAppContainer, createBottomTabNavigator, createMaterialTopTabNavigator } from "react-navigation";
import FooterTabComponent from './app/components/footer-tab/footer-tab.component';
import HeaderTabComponent from './app/components/header-tab/header-tab.component';
import SearchPreferenceScreen from './app/screens/search/search-preference.screen';
import SearchResultScreen from './app/screens/search/search-result.screen';
import ChatListScreen from './app/screens/chat/chat-list.screen';

export const FooterNavigator = createBottomTabNavigator(
  {
    SearchByPreference: { screen: SearchPreferenceScreen },
    SearchResult: { screen: SearchResultScreen },
    ChatList: { screen: ChatListScreen },
    Me: { screen: ProfileScreen },
  },
  {
    // backBehavior: 'none',
    tabBarComponent: (props) => (<FooterTabComponent navigation={props.navigation} />),
  }
);

const MainNavigator = createStackNavigator(
  {
    Login: { screen: LoginScreen },
    Home: { screen: FooterNavigator },
    Profile: { screen: ProfileScreen },
    Chat: { screen: ChatScreen },
  },
  {
    defaultNavigationOptions: {
      header: (props) => (<HeaderTabComponent navigation={props.navigation} />),
    },
  });

export default App = createAppContainer(MainNavigator);
import { AccessToken, GraphRequest, GraphRequestManager } from 'react-native-fbsdk';
import { Profile } from '../models/profile';
import { AsyncStorage } from 'react-native';
import { PROFILE } from '../constants/profile.constant';
import { ProfileService } from './profile.service';

export class FacebookService {
  static PERMISSION = ['public_profile', 'email', 'user_gender', 'user_birthday', 'user_location'];
  static FIELDS = 'name,email,gender,birthday,location{location},picture.type(large)';

  static getUserProfile() {
    return AccessToken.getCurrentAccessToken().then((data) => {
      return new Promise((resolve, reject) => {
        const responseInfoCallback = (error, profile) => {
          if (error) {
            console.error(error);
            reject(new Profile());
          } else {
            const cached = new Profile(profile);
            AsyncStorage.setItem(PROFILE.KEY.CACHED_USER, JSON.stringify(cached))
              .then(() => {
                ProfileService.current = cached;
                resolve(cached);
              });
          }
        };

        const accessToken = data.accessToken;
        const infoRequest = new GraphRequest(
          '/me',
          {
            accessToken,
            parameters: {
              fields: { string: FacebookService.FIELDS },
            },
          },
          responseInfoCallback,
        );
        new GraphRequestManager().addRequest(infoRequest).start();
      });
    });
  }
}
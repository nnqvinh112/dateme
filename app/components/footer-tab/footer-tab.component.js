import React, { Component } from 'react';
import { Footer, FooterTab, Button, Icon, Text, StyleProvider } from 'native-base';
import { ROUTE } from '../../constants/route.constant';
// import customVariables from 'src/snative-base-theme/variables/variable';

export default class FooterTabComponent extends Component {
    state = {
        tab: ROUTE.SEARCH_PREFERENCE,
    }

    constructor(props) {
        super(props);
    }

    goto(tab) {
        this.setState({ tab });
        this.props.navigation.navigate(tab);

    }

    render() {
        const { tab } = this.state;
        return (
            // <StyleProvider style={getTheme(material)}>
                <Footer>
                    <FooterTab>
                        <Button active={tab == ROUTE.ME} onPress={() => this.goto(ROUTE.ME)} >
                            <Icon name="person" />
                            <Text>Me</Text>
                        </Button>
                        <Button active={tab == ROUTE.SEARCH_PREFERENCE} onPress={() => this.goto(ROUTE.SEARCH_PREFERENCE)}>
                            <Icon name="search" />
                            <Text>Search</Text>
                        </Button>
                        <Button active={tab == ROUTE.CHAT_LIST} onPress={() => this.goto(ROUTE.CHAT_LIST)}>
                            <Icon name="chatbubbles" />
                            <Text>Chat</Text>
                        </Button>
                    </FooterTab>
                </Footer>
            // </StyleProvider>
        );
    }
};
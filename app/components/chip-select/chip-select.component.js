import React, { Component } from 'react';
import { Container, Content, View } from 'native-base';
import ChipComponent from './chip.component';

const styles = {
    container: {
        flexDirection: 'row',
        flexWrap: 'wrap',
        margin: -2,
        paddingTop: 10,
        paddingBottom: 10,
    }
}
export default class ChipSelectComponent extends Component {
    constructor(props) {
        super(props);
    }

    itemSelected(item) {
        const value = item.value;
        const { toggleable, selectedValue, multiple } = this.props;
        if (multiple) {
            const results = [];
            if (selectedValue instanceof Array ){
                results.push(...selectedValue);
            } else {
                results.push(selectedValue);
            }

            const index = results.findIndex(i => i === value);
            if (index >= 0) {
                results.splice(index, 1);
            } else {
                results.push(value);
            }
            return this.props.onValueChange(results);
        }

        if (toggleable && value && value === selectedValue) {
            return this.props.onValueChange(null);
        }

        return this.props.onValueChange(value);
    }

    isActive(item) {
        const {selectedValue} = this.props;
        if (selectedValue instanceof Array) {
            return !!selectedValue.find(i => i === item.value);
        }
        return selectedValue === item.value;
    }

    render() {
        const { data, selectedValue } = this.props;
        if (!data || !data.length) {
            return null;
        }
        return (
            <View style={styles.container}>
                {data.map(i => {
                    return <ChipComponent
                        key={i.value}
                        data={i}
                        onSelect={() => { this.itemSelected(i) }}
                        active={this.isActive(i)}
                    />
                })}
            </View>
        );
    }
}
import React, { Component } from 'react';
import { View, AsyncStorage } from 'react-native';
import { LoginButton } from 'react-native-fbsdk';
import { FacebookService } from '../../services/facebook.service';
import { PROFILE } from '../../constants/profile.constant';

export default class FBLoginButtonComponent extends Component {
  constructor(props) {
    super(props);
  }

  onLoggedIn(error, result) {
    if (error) {
      alert("Login failed with error: " + error);
    } else if (result.isCancelled) {
      alert("Login was cancelled");
    } else {
      FacebookService.getUserProfile().then(profile => {
        if (this.props.onLoggedIn) {
          this.props.onLoggedIn(profile);
        }
      });
    }
  }

  onLoggedOut(error) {
    AsyncStorage.removeItem(PROFILE.KEY.CACHED_USER)
      .then(() => this.props.onLoggedOut());
      
    if (this.props.onLoggedOut) {
      this.onLoggedOut();
    }
  }

  render() {
    return (
      <LoginButton
        readPermissions={FacebookService.PERMISSION}
        onLoginFinished={(error, result) => this.onLoggedIn(error, result)}
        onLogoutFinished={(error) => this.onLoggedOut(error)}
      />
    );
  }
};

module.exports = FBLoginButtonComponent;
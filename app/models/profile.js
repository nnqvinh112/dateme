import { Location } from "./location";
import { Preference } from './preference';

export class Profile extends Preference {
    Id = '';
    Name = '';
    Gender = '';
    Email = '';
    Birthday = new Date();
    Location = new Location();
    Picture = '';
    Biography = '';
    Preference = new Preference();

    constructor(item) {
        super(item);
        if (item) {
            this.Id = item.id || item.Id;
            this.Gender = item.gender || item.Gender;
            this.Name = item.name || item.Name;
            this.Email = item.email || item.Email;
            const { location, picture } = item;
            this.Location = location ? new Location(location.location) : new Location();
            this.Birthday = new Date(item.birthday);
            this.Picture = (picture && picture.data) ? picture.data.url : '';
        }
    }
}
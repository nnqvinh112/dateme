import { FirebaseService } from "./firebase.service";
import { FIREBASE } from "../constants/firebase.constant";
import { ProfileService } from "./profile.service";
import { objectToArrayWithKeyAsProp, jsonToConversations } from "../commons/mapping.common";
import { ChatMessage } from "../models/chat-message";
import { Conversation } from "../models/conversation";
import * as _ from 'lodash';
import { Profile } from "../models";

export class ChatService {
    /**
     * 
     * @param {(res: Conversation[]) => any} callBack 
     */
    static getAllConversations(callBack) {
        return FirebaseService.on(FIREBASE.KEYS.CONVERSATION, snapshot => {
            callBack(jsonToConversations(snapshot.val()) || []);
        });
    }

    /**
     * 
     * @param {Profile} profile 
     * @param {(res: Conversation[]) => any} callBack 
     */
    static getConversationOf(profile, callBack) {
        return ChatService.getAllConversations((res) => {
            callBack(res.filter(i => i.UserIds && i.UserIds.includes(profile.Id)));
        });
    }

    /**
     * 
     * @param {*} profile 
     * @param {(res: Conversation) => any} callBack 
     */
    static getConversationWith(profile, callBack) {
        return FirebaseService.on(FIREBASE.KEYS.CONVERSATION, snapshot => {
            /** @type {Conversation[]} */
            let converseArr = snapshot.val();
            if (!converseArr) {
                return callBack();
            }

            converseArr = objectToArrayWithKeyAsProp(converseArr);
            converseArr = converseArr.filter(i => i.Messages);

            return ProfileService.getCachedProfile().then(cached => {
                const converse = converseArr.find(i => {
                    if (i.UserIds.includes(cached.Id) && i.UserIds.includes(profile.Id)) {
                        return true;
                    }
                    return false;
                });

                if (!converse) {
                    return callBack();
                }

                let { Messages } = converse;
                Messages = objectToArrayWithKeyAsProp(Messages).map(j => new ChatMessage(j));
                Messages = _.sortBy(Messages, 'CreatedTime');
                converse.Messages = Messages;
                return callBack(converse);
            })
        });
    }

    /**
     * 
     * @param {Conversation} conversation 
     * @param {*} callBack 
     */
    static getMessagesWith(conversation, callBack) {
        if (conversation) {
            return callBack(conversation.Messages || []);
        }
        return callBack([]);
    }

    /**
     * 
     * @param {*} receiverId 
     */
    static async createConversationWith(receiverId) {
        const cached = await ProfileService.getCachedProfile();
        const conversation = new Conversation({
            UserIds: [cached.Id, receiverId],
        });

        const id = await FirebaseService.push(
            FIREBASE.KEYS.CONVERSATION,
            conversation,
        );

        conversation.Id = id;
        return conversation;
    }

    static sendMessageTo(receiverId, message, conversation) {
        if (!message || !message.Message) {
            return Promise.reject();
        }
        let request = Promise.resolve(conversation);
        if (!conversation || !conversation.Id) {
            request = request.then(() => this.createConversationWith(receiverId));
        }

        return request.then(converse => {
            const path = `${FIREBASE.KEYS.CONVERSATION}/${converse.Id}/Messages`;
            request = request.then(() => FirebaseService.push(path, message));
        });
    }

    /**
     * @returns {Promise<Profile[]>}
     * @param {Conversation[]} conversations 
     */
    static async getPeopleConversedWith(conversations) {
        if (!conversations) {
            return Promise.resolve([]);
        }
        let userIds = conversations
            .filter(i => i.Messages && i.Messages.length > 0)
            .map(i => i.UserIds);
        const current = await ProfileService.getCachedProfile();
        userIds = _.flatten(userIds);
        userIds = _.uniq(userIds);
        userIds = _.filter(userIds, (i) => i !== current.Id);
        return ProfileService.getByIds(userIds);
    }
}
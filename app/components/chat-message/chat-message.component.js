import React, { Component } from 'react';
import { Image, StyleSheet } from 'react-native';
import { Card, CardItem, Body, Text } from 'native-base';
import { ProfileService } from '../../services/profile.service';
import { STYLE } from '../../constants/style.constant';

const baseStyles = {
  message: {
    padding: 10,
    maxWidth: '80%',
    borderRadius: 10,
  }
}
const styles = StyleSheet.create({
  cardItem: {

  },
  body: {

  },
  sender: {
    ...baseStyles.message,
    backgroundColor: STYLE.COLOR.SECONDARY,
    marginLeft: 'auto',
  },
  receiver: {
    ...baseStyles.message,
    backgroundColor: STYLE.COLOR.PRIMARY,
    marginRight: 'auto',
  },
});

export default class ChatMessageComponent extends Component {
  constructor(props) {
    super(props);
  }

  isSenderMessage() {
    const { sender } = this.props;
    if (sender) {
      return sender.Id == this.props.data.SenderId;
    }
    return false;
  }

  getMessageStyle() {
    return this.isSenderMessage() ? styles.sender : styles.receiver;
  }

  render() {
    const {data} = this.props;
    if (!data) {
      return null;
    }
    return (
        <Card transparent>
          <CardItem style={styles.cardItem}>
            <Body style={styles.body}>
              <Text style={this.getMessageStyle()}>
                {data.Message}
              </Text>
            </Body>
          </CardItem>
        </Card>
    );
  }
};
export const ROUTE = {
    LOGIN: 'Login',
    HOME: 'Home',
    ME: 'Me',
    PROFILE: 'Profile',
    CHAT: 'Chat',
    CHAT_LIST: 'ChatList',
    SEARCH: 'Search',
    SEARCH_RESULT: 'SearchResult',
    SEARCH_PREFERENCE: 'SearchByPreference',
    SEARCH_LOCATION: 'SearchByLocation',
}
export class ChatMessage {
    CreatedTime = new Date();
    Message = '';
    SenderId = '';

    constructor(item) {
        if (item) {
            this.CreatedTime = item.CreatedTime || new Date();
            this.Message = item.Message;
            this.SenderId = item.SenderId;
        }
    }
}
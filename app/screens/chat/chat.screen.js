import React, { Component } from 'react';
import { Container, Content, Footer, FooterTab, Item, Input, Icon, Button, Text, Spinner } from 'native-base';
import { StyleSheet, View } from 'react-native';
import { Profile } from '../../models';
import { ChatService } from '../../services/chat.service';
import ChatMessageComponent from '../../components/chat-message/chat-message.component';
import { STYLE } from '../../constants/style.constant';
import { ChatMessage } from '../../models/chat-message';
import { ProfileService } from '../../services/profile.service';
import Dimensions from 'Dimensions';

const vh = Dimensions.get('window').height;
const styles = StyleSheet.create({
    content: {

    },
    footer: {
        backgroundColor: STYLE.COLOR.LIGHT,
        padding: 10,
    },
    input: {
        width: '100%',
    },
    centerView: {
        height: vh,
        justifyContent: 'center',
        alignItems: 'center',
    }
});

export default class ChatScreen extends Component {
    constructor(props) {
        super(props);
    }

    textInput;
    state = {
        /** @type {Profile} */
        receiver: null,
        sender: null,
        /** @type {ChatMessage[]} */
        messages: [],
        conversation: null,
        isLoading: false,
        text: '',
    }

    async componentDidMount() {
        const { navigation } = this.props;
        const receiver = navigation.getParam('receiver');

        this.setState({ receiver });

        if (receiver && receiver.Id) {
            this.setState({ isLoading: true });
            const sender = await ProfileService.getCachedProfile();
            this.setState({sender});
            ChatService.getConversationWith(receiver, conversation => {
                if (conversation) {
                    this.setState({
                        conversation,
                        messages: conversation.Messages,
                    });
                }
                this.setState({ isLoading: false });
            });
        }
    }

    async onSubmit() {
        const text = this.state.text.trim();
        if (text) {
            const { sender } = this.state;
            const message = new ChatMessage({
                SenderId: sender.Id,
                Message: text,
            })
            const res = await ChatService.sendMessageTo(this.state.receiver.Id, message, this.state.conversation)
            this.setState({ sender, text: '' });
            this.textInput._root.clear();
        }
    }

    renderMessages() {
        const { isLoading, messages, receiver, sender } = this.state;
        if (isLoading) {
            return <View style={styles.centerView}><Spinner color={STYLE.COLOR.PRIMARY} /></View>

        }
        if (!messages || !messages.length) {
            return (
                <View style={styles.centerView}>
                    <Text>You and {receiver.Name}</Text>
                    <Text>haven't had any conversation yet</Text>
                </View>
            )
        }
        return messages.map((i, index) => <ChatMessageComponent key={index} data={i} sender={sender}></ChatMessageComponent>)
    }
    render() {
        const { receiver } = this.state;
        if (!receiver || !receiver.Id) {
            return null;
        }
        return (
            <Container>
                <Content style={styles.content}>
                    {this.renderMessages()}
                </Content>
                <Footer>
                    <FooterTab style={styles.footer}>
                        <Item rounded style={styles.input}>
                            <Input placeholder='Enter message here...'
                                ref={(input) => { this.textInput = input; }}
                                value={this.state.text} autoCorrect={false}
                                onChangeText={(text) => this.setState({ text })} />
                            <Button small transparent onPress={() => this.onSubmit()}>
                                <Icon name='send' ></Icon>
                            </Button>
                        </Item>
                    </FooterTab>
                </Footer>
            </Container>
        )
    }
}

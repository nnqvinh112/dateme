import React, { Component } from 'react';
import { StyleSheet, ImageBackground, View, AsyncStorage } from 'react-native';
import { Container, Header, Content, Text, H3, H1 } from 'native-base';
import FBLoginButtonComponent from '../../components/fb-login-button/fb-login-button.component';
import { ROUTE } from '../../constants/route.constant';
import { PROFILE } from '../../constants/profile.constant';
import { Profile } from '../../models';

const styles = StyleSheet.create({
  background: {
    flex: 1,
    textAlign: 'center',
    justifyContent: 'center',
    alignItems: 'center',
    position: 'relative',
  },
  slogan: {
    position: 'absolute',
    bottom: '30%',
    color: 'white',
  },
  brand: {
    position: 'absolute',
    color: 'white',
    bottom: '32.5%',
  },
  login: {
    position: 'absolute',
    bottom: '20%',
  }
})
export default class LoginScreen extends Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    AsyncStorage.getItem(PROFILE.KEY.CACHED_USER).then(res => {
      if (res) {
        this.onLoggedIn();
      }
    })
  }

  /**
   *
   * @param {Profile} profile
   */
  onLoggedIn(profile) {
    console.log(profile);
    this.props.navigation.navigate(ROUTE.HOME);
  }

  render() {
    return (
      <Container>
        <ImageBackground source={require('../../assets/images/login.jpg')} style={styles.background}>
          <H1 style={styles.brand}>Date.Me</H1>
          <H3 style={styles.slogan}>Connect to your Soulmate</H3>
          <View style={styles.login}>
            <FBLoginButtonComponent onLoggedIn={(profile) => this.onLoggedIn(profile)} />
          </View>
        </ImageBackground>
      </Container>
    )
  }
}

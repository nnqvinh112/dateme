import React, { Component } from 'react';
import { Container, Item, Label, Input, Content, Card, CardItem, DatePicker, Picker, Textarea, Form, H3, Icon, Grid, Row } from 'native-base';
import { StyleSheet, Image, Dimensions, View } from 'react-native';
import { ProfileService } from '../../services/profile.service';
import { PREFERENCE_OPTIONS } from '../../constants/preference.constant';
import { ROUTE } from '../../constants/route.constant';
import MultiSlider from '@ptomasroos/react-native-multi-slider';
import FBLoginButtonComponent from '../../components/fb-login-button/fb-login-button.component';
import { capitalize } from '../../commons/utility.common';
import { Profile } from '../../models';
import * as _ from 'lodash';

const win = Dimensions.get('window');
const avatarFactor = 0.90;
const styles = StyleSheet.create({
  content: {

  },
  avatar: {
    flex: avatarFactor,
    height: win.width * avatarFactor,
    alignSelf: 'stretch',
    marginLeft: 'auto',
    marginRight: 'auto',
  },
  avatarContainer: {
    paddingLeft: 0,
    paddingRight: 0,
    paddingTop: 0,
  },
  textarea: {
    width: "100%",
  },
  centerView: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  heightValue: {
    textAlign: 'right',
  },
  heightItem: {
    borderColor: 'transparent',
    flex: 1,
  },
  heightRow: {
    justifyContent: 'center',
  },
  item: {
    borderColor: 'transparent',
  }
});

export default class ProfileScreen extends Component {
  constructor(props) {
    super(props);
  }

  textInput;
  state = {
    isLoading: false,
    isDisabled: false,
    isPreferenceHidden: false,
    /** @type {Profile} */
    profile: null,
    height: [PREFERENCE_OPTIONS.HEIGHT.DEFAULT],
    gender: null,
    education: null,
    profession: null,
    /**
     * Current user that logged in
     * @type {Profile} */
    loggedProfile: null,
    bodyShape: [PREFERENCE_OPTIONS.BODY_SHAPE.LEAN],
  }

  async componentDidMount() {
    const { navigation } = this.props;
    const profile = navigation.getParam('profile');
    this.setState({ profile, isLoading: true });
    const loggedProfile = await ProfileService.getCachedProfile();
    if (profile && profile.Id) {
      this.setState({ profile, loggedProfile });
    } else {
      this.setState({ profile: loggedProfile, loggedProfile });
    }
    this.setState({ isLoading: false, isDisabled: profile.Id !== loggedProfile.Id });
  }

  renderBodyShape() {
    const { BODY_SHAPE } = PREFERENCE_OPTIONS;
    const bodyShape = this.state.bodyShape;
    const items = this.mapLabelValue(BODY_SHAPE);
    const input = {
      items,
      field: 'bodyShape',
      selectedValue: bodyShape,
      label: 'Body Shape',
    }
    return this.renderPickerField(input);
  }

  renderEducation() {
    const { EDUCATION } = PREFERENCE_OPTIONS;
    const education = this.state.education;
    const items = this.mapLabelValue(EDUCATION);
    const input = {
      items,
      field: 'education',
      selectedValue: education,
      label: 'Education',
    }
    return this.renderPickerField(input);
  }

  renderProfession() {
    const { PROFESSION } = PREFERENCE_OPTIONS;
    const profession = this.state.profession;
    const items = this.mapLabelValue(PROFESSION);
    const input = {
      items,
      field: 'profession',
      selectedValue: profession,
      label: 'Profession',
    }
    return this.renderPickerField(input);
  }

  renderHeight() {
    const { height, isDisabled } = this.state;
    const heightChange = (h) => {
      if (h != height) {
        this.setState({ height: h })
      }
    }
    return (
        <Grid>
          <Row >
            <Item fixedLabel  style={styles.heightItem}>
              <Label>Height</Label>
              <Input style={styles.heightValue} disabled={true} value={height[0] + ' cm'} />
            </Item>
          </Row>
          {!isDisabled && (
            <Row style={styles.heightRow}>
              <MultiSlider
                values={height}
                isMarkersSeparated={false}
                min={PREFERENCE_OPTIONS.HEIGHT.MIN}
                max={PREFERENCE_OPTIONS.HEIGHT.MAX}
                step={1}
                onValuesChangeFinish={(height) => heightChange(height)}
              />
            </Row>
          )}
        </Grid>
    );
  }

  renderPickerField(input) {
    const {isDisabled} = this.state;
    updateValue = (value, field) => {
      const obj = {};
      obj[field] = value;
      this.setState(obj);
    }
    return (
      <Item fixedLabel style={styles.item}>
        <Label>{input.label}</Label>
        <Picker
          enabled={!isDisabled}
          data={input.items}
          selectedValue={input.selectedValue}
          multiple={true}
          onValueChange={(value) => updateValue(value, input.field)}
        >
          {this.renderPickerItems(input.items)}
        </Picker>
      </Item>
    );
  }

  renderPickerItems(items) {
    return items.map((i, index) => {
      return <Picker.Item key={index} label={capitalize(i.label)} value={i.value} />
    });
  }

  mapLabelValue(obj) {
    const keys = Object.keys(obj);
    return keys.map(i => ({
      label: obj[i],
      value: obj[i],
    }));
  }

  logout() {
    const { navigation } = this.props;
    navigation.navigate(ROUTE.LOGIN);
  }

  render() {
    const { isDisabled, profile, loggedProfile } = this.state;
    if (!profile || !profile.Id || !loggedProfile || !loggedProfile.Id) {
      return null;
    }
    return (
      <Container>
        <Content>
          <Card transparent>
            <CardItem style={styles.avatarContainer}>
              <Image style={styles.avatar} source={{ uri: profile.Picture }}></Image>
            </CardItem>
            <CardItem cardBody>
              <Form>
                <Item stackedLabel style={styles.item}>
                  <Label>Profile Name</Label>
                  <Input disabled={isDisabled} value={profile.Name} onChangeText={Name => this.setState({ profile: { ...profile, Name } })} />
                </Item>
                <Item stackedLabel style={styles.item}>
                  <Label>Email Address</Label>
                  <Input disabled={isDisabled} value={profile.Email} onChangeText={Email => this.setState({ profile: { ...profile, Email } })} />
                </Item>
                <Item fixedLabel style={styles.item}>
                  <Label>Birthday</Label>
                  <DatePicker disabled={isDisabled} defaultDate={new Date(profile.Birthday)} onDateChange={Birthday => this.setState({ profile: { ...profile, Birthday } })} />
                </Item>
                <Item fixedLabel style={styles.item}>
                  <Label>Gender</Label>
                  <Picker
                    enabled={false}
                    iosHeader="Select one"
                    mode="dropdown"
                    selectedValue={profile.Gender}
                  >
                    <Item label="Male" value={PREFERENCE_OPTIONS.GENDER.MALE} />
                    <Item label="Female" value={PREFERENCE_OPTIONS.GENDER.FEMALE} />
                  </Picker>
                </Item>

                {this.renderHeight()}
                {this.renderBodyShape()}
                {this.renderEducation()}
                {this.renderProfession()}
              </Form>
            </CardItem>

            {(profile.Id === loggedProfile.Id) && (
              <CardItem>
                <View style={styles.centerView}>
                  <FBLoginButtonComponent onLoggedOut={() => this.logout()} />
                </View>
              </CardItem>
            )}
          </Card>
        </Content>
      </Container>
    )
  }
}

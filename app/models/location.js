export class Location {
    City = '';
    Country = '';
    Latitude = 0;
    Longitude = 0;

    constructor(item) {
        if (item) {
            this.City = item.city;
            this.Country = item.country;
            this.Latitude = item.latitude;
            this.Longitude = item.longitude;
        }
    }
}
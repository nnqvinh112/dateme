import { ChatMessage } from "./chat-message";

export class Conversation {
    UserIds = [];
    /** @type {ChatMessage[]} */
    Messages = [];
    CreatedTime = new Date();

    constructor(item) {
        if (item) {
            this.UserIds = item.UserIds || []; 
            this.Messages = item.Messages || [];
            this.CreatedTime = item.CreatedTime || new Date();
        }
    }
}
import React, { Component } from 'react';
import { StyleSheet } from 'react-native';
import { Header, FooterTab, Button, Icon, Text, Body } from 'native-base';
import { ROUTE } from '../../constants/route.constant';

const styles = StyleSheet.create({
    title: {
        textAlign: 'center',
        marginLeft: 'auto',
        marginRight: 'auto',
        color: 'white',
    }
});

export default class FooterTabComponent extends Component {
    state = {
        tab: ROUTE.SEARCH,
    }

    constructor(props) {
        super(props);
    }

    goto(tab) {
        this.setState({ tab });
        this.props.navigation.navigate(tab);
    }

    render() {
        const {tab} = this.state;
        return null;
        // (
        //     <Header>
        //         <Body>
        //             <Text style={styles.title}>Date.Me</Text>
        //         </Body>
        //     </Header>
        // );
    }
};
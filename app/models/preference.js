import { PREFERENCE_OPTIONS } from "../constants/preference.constant";

export class Preference {
    Gender = PREFERENCE_OPTIONS.GENDER.MALE;
    BodyShape = PREFERENCE_OPTIONS.BODY_SHAPE.LEAN;
    Height = -1;
    Education = '';
    Profession = '';

    constructor() {

    }
}
import { Conversation } from "../models/conversation";
import { ChatMessage } from "../models/chat-message";
import * as _ from 'lodash';

export function objectToArrayWithKeyAsProp(object, propName) {
    if (!object) {
        throw `Invalid input object: ${object}`;
    }

    const prop = propName ? propName : 'Id';
    const keys = Object.keys(object);
    return keys.map((key) => {
        const item = { ...object[key] };
        item[prop] = key;
        return item;
    });
}

/**
 * @returns {ChatMessage[]}
 * @param {*} json 
 */
export function jsonToChatMessages(json) {
    if (!json) {
        return [];
    }

    /** @type {ChatMessage[]} */
    let messArr = objectToArrayWithKeyAsProp(json);
    if (!messArr) {
        return [];
    }

    messArr = messArr.filter(i => i.Message);
    messArr = _.sortBy(messArr, 'CreatedTime');
    return messArr;
}

/**
 * @returns {Conversation[]}
 * @param {*} json 
 */
export function jsonToConversations(json) {
    if (!json) {
        return [];
    }

    /** @type {Conversation[]} */
    let converseArr = objectToArrayWithKeyAsProp(json);
    if (!converseArr) {
        return [];
    }

    converseArr = converseArr.filter(i => i.Messages);
    converseArr.forEach(converse => {
        let { Messages } = converse;
        if (Messages) {
            Messages = jsonToChatMessages(Messages);
        }
        converse.Messages = jsonToChatMessages(Messages) || [];
    })
    return converseArr;
}

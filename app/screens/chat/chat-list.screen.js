import React, { Component } from 'react';
import { Container, Content, Text, Header, List, ListItem, Left, Body, Right, Thumbnail, View, Spinner } from 'native-base';
import { ChatService } from '../../services/chat.service';
import { ProfileService } from '../../services/profile.service';
import { StyleSheet, Dimensions } from 'react-native';
import { STYLE } from '../../constants/style.constant';
import { Conversation } from '../../models/conversation';
import { Profile } from '../../models';
import { ROUTE } from '../../constants/route.constant';
import * as _ from 'lodash';
import * as moment from 'moment';

const vh = Dimensions.get('window').height;
const styles = StyleSheet.create({
    centerView: {
        height: vh,
        width: '100%',
        justifyContent: 'center',
        alignItems: 'center',
    }
});

export default class ChatListScreen extends Component {
    state = {
        isLoading: false,
        /** @type {Conversation[]} */
        conversations: [],
        /** @type {Profile[]} */
        profiles: [],
    }

    constructor(props) {
        super(props);
    }

    async componentDidMount() {
        this.setState({ isLoading: true });
        const profile = await ProfileService.getCachedProfile();
        ChatService.getConversationOf(profile, (conversations) => {
            ChatService.getPeopleConversedWith(conversations).then(
                profiles => this.setState({ conversations, profiles, isLoading: false })
            );
        });
    }

    goToChatScreen(profile) {
        const { navigation } = this.props;
        navigation.navigate(ROUTE.CHAT, { receiver: profile });
    }

    goToProfileScreen(profile) {
        const { navigation } = this.props;
        navigation.navigate(ROUTE.PROFILE, { profile });
    }

    renderConversation() {
        const { conversations, profiles } = this.state;
        if (conversations && profiles && conversations.length === profiles.length) {
            return (
                profiles.map((i, index) => {
                    const converse = conversations.find(x => x.UserIds.indexOf(i.Id) > -1);
                    const lastMess = _.last(converse.Messages);
                    const lastMessTime = moment.utc(lastMess.CreatedTime).calendar();
                    return (<ListItem avatar key={i.Id} onPress={() => this.goToChatScreen(i)}>
                        <Left>
                            <Thumbnail source={{ uri: i.Picture }} />
                        </Left>
                        <Body>
                            <Text style={{ color: STYLE.COLOR.PRIMARY, fontWeight: '500' }}>{i.Name}</Text>
                            <Text note>{lastMess.Message}</Text>
                        </Body>
                        <Right>
                            <Text note>{lastMessTime}</Text>
                        </Right>
                    </ListItem>)
                })
            )
        }
        return (<View style={styles.centerView}>
            <Text>
                You haven't started any conversations
            </Text>
        </View>);
    }

    render() {
        const { isLoading } = this.state;
        return (
            <Container>
                <Content>
                    {isLoading && (
                        <View style={styles.centerView}><Spinner color={STYLE.COLOR.PRIMARY} /></View>
                    )}

                    {!isLoading && (
                        <List>{this.renderConversation()}</List>
                    )}
                </Content>
            </Container>
        );
    }
}
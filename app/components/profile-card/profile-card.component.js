import React, { Component } from 'react';
import { Image, StyleSheet, Dimensions } from 'react-native';
import { Card, CardItem, Body, Text, Button, Icon, H3, Grid, Row, Col } from 'native-base';
import { ROUTE } from '../../constants/route.constant';

const win = Dimensions.get('window');
const styles = StyleSheet.create({
  image: {
    width: win.width * 0.95,
    height: win.width * 0.95,
  },
  buttonBar: {
    position: 'absolute',
    bottom: '2.5%',
    left: 0,
    right: 0,
    justifyContent: 'center',
    flexDirection: 'row',
  },
  interactButton: {
    margin: 4,
  },
  card: {
    margin: '5%',
  }
});

export default class ProfileCardComponent extends Component {
  constructor(props) {
    super(props);
  }

  goToChatScreen() {
    const { navigation, profile } = this.props;
    navigation.navigate(ROUTE.CHAT, { receiver: profile });
  }

  goToProfileScreen() {
    const { navigation, profile } = this.props;
    navigation.navigate(ROUTE.PROFILE, { profile });
  }

  render() {
    if (!this.props.profile) {
      return null;
    }
    const { profile } = this.props;
    return (
      <Card style={styles.card}>
        <CardItem header bordered button onPress={() => this.goToProfileScreen()}>
          <H3>{profile.Name}</H3>
        </CardItem>
        <CardItem cardBody>
          <Image source={{ uri: profile.Picture }} style={styles.image} />
          <Body style={styles.buttonBar}>
            <Button rounded primary style={styles.interactButton} onPress={() => this.goToProfileScreen()}>
              <Icon type="FontAwesome" name="info-circle"></Icon>
            </Button>
            {/* <Button rounded primary style={styles.interactButton}>
              <Icon type="FontAwesome" name="heart"></Icon>
            </Button> */}
            <Button rounded primary style={styles.interactButton}>
              <Icon type="FontAwesome" name="comment" onPress={() => this.goToChatScreen()}></Icon>
            </Button>
          </Body>
        </CardItem>
        <CardItem>
          <Grid>
            <Row>
              <Col style={{ flexDirection: 'row' }}>
                <Icon type="Ionicons" name="school"></Icon>
                <Text>{profile.Education}</Text>
              </Col>
              <Col style={{ flexDirection: 'row' }}>
                <Icon type="FontAwesome" name="map-marker"></Icon>
                <Text>{profile.Location.City}</Text>
              </Col>
            </Row>
          </Grid>
        </CardItem>
        <CardItem>
          <Icon type="Ionicons" name="briefcase"></Icon>
          <Text>{profile.Profession}</Text>
        </CardItem>
      </Card>
    );
  }
};
import { FIREBASE } from "../constants/firebase.constant";
import firebase from 'firebase';

if (!firebase.apps.length) {
  firebase.initializeApp(FIREBASE.CONFIG);
}

// firebase.auth()
//   .signInAnonymously()
//   .then(credential => {
//     if (credential) {
//       console.log('default app user ->', credential.user.toJSON());
//     }
//   });

export class FirebaseService {
  static set(path, data) {
    return firebase.database().ref(path).set(JSON.parse(JSON.stringify(data)))
      .then((res) => {
        return res;
      })
      .catch((error) => {
        console.error('Error ', error);
        return null;
      })
  }

  static push(path, data) {
    const dataRef = firebase.database().ref(path).push(JSON.parse(JSON.stringify(data)));
    return dataRef
      .then(() => {
        return dataRef.key;
      })
      .catch((error) => {
        console.error('Error ', error);
      })
  }

  static put(path, key, data) {
    return firebase.database().ref(path).child(key).set(JSON.parse(JSON.stringify(data)))
      .then((res) => {
        return res;
      })
      .catch((error) => {
        console.error('Error ', error);
      })
  }

  static delete(path) {
    return firebase.database().ref(path).remove();
  }

  static update(path, data) {
    return firebase.database().ref(path).update(JSON.parse(JSON.stringify(data)));
  }

  static once(path) {
    return firebase.database().ref(path).once('value', function (snapshot) {
      return snapshot.val();
    });
  }

  static on(path, callBack) {
    return firebase.database().ref(path).on('value', (snapshot) => callBack(snapshot));
  }
}

import React, { Component } from 'react';
import { StyleSheet } from 'react-native';
import { Container, Content, Text, Button } from 'native-base';
import { STYLE } from '../../constants/style.constant';

const styles = StyleSheet.create({
    chip: {
        margin: 2,
    },
    label: {
        paddingLeft: 12.5,
        paddingRight: 12.5,
    }
});
export default class ChipComponent extends Component {

    constructor(props) {
        super(props);
    }

    itemPressed() {
        this.props.onSelect(this.props.data);
    }

    render() {
        const { data, active } = this.props;
        return (
            <Button rounded small bordered={!active} style={styles.chip} onPress={() => this.itemPressed()}>
                <Text style={styles.label}>{data.label}</Text>
            </Button>
        );
    }
}
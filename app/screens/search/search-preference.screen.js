import React, { Component } from 'react';
import { StyleSheet } from 'react-native';
import { Container, Content, Footer, Grid, Col, Card, CardItem, Text, Label, Button, Picker, Item, Input, Row, Form, Icon } from 'native-base';
import { PREFERENCE_OPTIONS } from '../../constants/preference.constant';
import { ROUTE } from '../../constants/route.constant';
import { Preference } from '../../models/preference';
import ChipSelectComponent from '../../components/chip-select/chip-select.component';
import { ProfileService } from '../../services/profile.service';
import MultiSlider from '@ptomasroos/react-native-multi-slider';
import { capitalize } from '../../commons/utility.common';

const styles = StyleSheet.create({
  item: {
    borderColor: 'transparent',
    flex: 1,
  },
  form: {

  },
  grid: {
    marginTop: 10,
  },
  searchButton: {
    marginBottom: 15,
    textAlign: 'center',
    justifyContent: 'center',
    alignItems: 'center',
    width: '90%',
    marginLeft: 'auto',
    marginRight: 'auto',
  },
  heightMin: {
    textAlign: 'right',
    justifyContent: 'flex-end',
  }
});

export default class SearchPreferenceScreen extends Component {
  state = {
    height: [PREFERENCE_OPTIONS.HEIGHT.MIN, PREFERENCE_OPTIONS.HEIGHT.MAX],
    gender: null,
    education: null,
    profession: null,
    bodyShape: [PREFERENCE_OPTIONS.BODY_SHAPE.LEAN],
  }

  constructor(props) {
    super(props);
  }

  componentDidMount() {
    this.initializePreference()
  }

  async initializePreference() {
    // Set default searching gender to be opposite to the current user's gender
    const cached = await ProfileService.getCachedProfile();
    if (cached.Gender === PREFERENCE_OPTIONS.GENDER.MALE) {
      this.setState({ gender: PREFERENCE_OPTIONS.GENDER.FEMALE });
    }
    if (cached.Gender === PREFERENCE_OPTIONS.GENDER.FEMALE) {
      this.setState({ gender: PREFERENCE_OPTIONS.GENDER.MALE });
    }
  }

  search() {
    const { navigation } = this.props;
    const preference = this.createPreference();
    navigation.navigate(ROUTE.SEARCH_RESULT, { preference });
  }

  createPreference() {
    const { height, gender, education, profession, bodyShape } = this.state;
    const preference = new Preference();
    preference.Height = height;
    preference.Gender = gender;
    preference.BodyShape = bodyShape;
    preference.Education = education;
    preference.Profession = profession;
    return preference;
  }

  renderGender() {
    const { GENDER } = PREFERENCE_OPTIONS;
    const gender = this.state.gender;
    const keys = Object.keys(GENDER);
    return (
      <Item stackedLabel>
        <Label>Looking for</Label>
        <Grid style={styles.grid}>
          {keys.map((key, index) => (
            <Col key={index}>
              <Button block
                onPress={() => this.setState({ gender: GENDER[key] })}
                light={gender !== GENDER[key]}
              >
                <Text>{GENDER[key]}</Text>
              </Button>
            </Col>
          ))}
        </Grid>
      </Item>
    );
  }

  renderBodyShape() {
    const { BODY_SHAPE } = PREFERENCE_OPTIONS;
    const bodyShape = this.state.bodyShape;
    const items = this.mapLabelValue(BODY_SHAPE);

    return (
      <ChipSelectComponent
        data={items}
        selectedValue={bodyShape}
        multiple={true}
        onValueChange={(bodyShape) => this.setState({ bodyShape })}
      />
    );
  }

  renderEducation() {
    const { EDUCATION } = PREFERENCE_OPTIONS;
    const education = this.state.education;
    const items = this.mapLabelValue(EDUCATION);

    return (
      <ChipSelectComponent
        data={items}
        multiple={true}
        selectedValue={education}
        onValueChange={(education) => this.setState({ education })}
      />
    );
  }

  renderProfession() {
    const { PROFESSION } = PREFERENCE_OPTIONS;
    const profession = this.state.profession;
    const items = this.mapLabelValue(PROFESSION);

    return (
      <ChipSelectComponent
        data={items}
        multiple={true}
        selectedValue={profession}
        onValueChange={(profession) => this.setState({ profession })}
      />
    );
  }

  renderHeight() {
    const { height } = this.state;
    const touchDimensions = { height: 150, width: 150, borderRadius: 15, slipDisplacement: 200 };
    const heightChange = (value) => {
      if (value[0] != height[0] || value[1] != height[1]) {
        this.setState({ height: value })
      }
    }
    return (
        <Grid style={{flex: 1}}>
          <Row>
            <Item fixedLabel style={styles.item}>
              <Label>Height</Label>
              <Input style={{textAlign: 'right'}} disabled={true} value={height[0] + ' cm - ' + height[1] + ' cm'} />
            </Item>
          </Row>
          <Row style={{ justifyContent: 'center'}}>
              <MultiSlider
                values={height}
                touchDimensions={touchDimensions}
                min={PREFERENCE_OPTIONS.HEIGHT.MIN}
                max={PREFERENCE_OPTIONS.HEIGHT.MAX}
                step={1}
                onValuesChangeFinish={(value) => heightChange(value)}
              />
          </Row>
        </Grid>
    );
  }

  renderPickerItems(items) {
    return items.map((i, index) => {
      return <Picker.Item key={index} label={capitalize(i.label)} value={i.value} />
    });
  }

  mapLabelValue(obj) {
    const keys = Object.keys(obj);
    return keys.map(i => ({
      label: obj[i],
      value: obj[i],
    }));
  }

  render() {
    return (
      <Container>
        <Content>
          <Form style={styles.form}>
            {this.renderGender()}
            {this.renderHeight()}
            <Item stackedLabel style={styles.item}>
              <Label>Body Shape</Label>
              {this.renderBodyShape()}
            </Item>
            <Item stackedLabel style={styles.item}>
              <Label>Education</Label>
              {this.renderEducation()}
            </Item>
            <Item stackedLabel style={styles.item}>
              <Label>Profession</Label>
              {this.renderProfession()}
            </Item>
          </Form>

          <Button onPress={() => this.search()} style={styles.searchButton}>
            <Icon name="search"></Icon>
            <Text>Search</Text>
          </Button>
        </Content>
      </Container>
    );
  }
}
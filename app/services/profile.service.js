import DEFAULT_PROFILES from '../assets/data/profiles.default.json';
import { Profile } from '../models/index.js';
import { objectToArrayWithKeyAsProp } from "../commons/mapping.common";
import { Preference } from '../models/preference.js';
import { AsyncStorage } from 'react-native';
import { PROFILE } from '../constants/profile.constant.js';
import { FirebaseService } from './firebase.service';

class Params extends Preference {
    includeSelf = false;
}

export class ProfileService {
    static profiles = [];

    /**
     *
     * @returns {Promise<Profile>}
     */
    static getCachedProfile() {
        return AsyncStorage.getItem(PROFILE.KEY.CACHED_USER)
            .then(res => {
                if (!res) {
                    return new Profile();
                }
                return JSON.parse(res);
            });
    }

    /**
     *
     * @param {Params} params
     */
    static getAll(params) {
        /** @type {Promise<Profile[]>} */
        let request = Promise.resolve(DEFAULT_PROFILES);
        request = FirebaseService.once('profiles').then(snapshot => objectToArrayWithKeyAsProp(snapshot.val()));
        if (params) {
            Object.keys(params)
                .filter(key => params[key])
                .forEach(key => {
                if (key !== 'includeSelf') {
                    request = request.then(res => this.filterByProp(res, key, params[key]));
                }
            })

            if (!params.includeSelf) {
                request = request.then(async res => {
                    const current = await this.getCachedProfile();
                    return res.filter(i => i.Id != current.Id);
                });
            }
        }
        return request;
    }

    static getById(id) {

    }

    /**
     * @return {Promise<Profile[]>}
     * @param {string[]} ids
     */
    static getByIds(ids) {
        if (!ids) {
            return Promise.resolve([]);
        }
        return ProfileService.getAll({ Id: ids });
    }

    /**
     *
     * @param {Profile[]} list
     * @param {string} prop
     * @param {string | string[]} filterValue
     */
    static filterByProp(list, prop, filterValue) {
        if (filterValue instanceof Array) {
            filterValue = filterValue.filter(i => !!i);
        } else {
            filterValue = [filterValue];
        }
        if (!prop || !filterValue.length) {
            return list;
        }

        if (prop === 'Height') {
            return list.filter(x => x.Height >= filterValue[0] && x.Height <= filterValue[1]);
        }

        if (filterValue instanceof Array) {
            return list.filter(x => filterValue.find(y => y === x[prop]));
        }
        return list.filter(i => i[prop] === filterValue);
    }
}
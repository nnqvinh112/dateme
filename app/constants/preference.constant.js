export const PREFERENCE_OPTIONS = {
    GENDER: {
        MALE: 'male',
        FEMALE: 'female',
    },
    HEIGHT: {
        DEFAULT: 160,
        MIN: 100,
        MAX: 230,
    },
    BODY_SHAPE: {
        SKINNY: 'skinny',
        SLIM: 'slim',
        LEAN: 'lean',
        CHUBBY: 'chubby',
        FAT: 'fat',
    },
    SKIN_TONE: {
        WHITE: 'white',
        TAN: 'tan',
        DARK: 'dark',
    },
    EDUCATION: {
        HIGH_SCHOOL: 'high school',
        ASSOCIATE: 'associate',
        BACHELOR: 'bachelor',
        MASTER: 'master',
        DOCTORATE: 'doctorate',
    },
    PROFESSION: {
        STUDENT: 'student',
        FREELANCE: 'freelance',
        IT: 'programmer/it',
        BUSSINESS_OWNER: 'bussiness owner',
        FOOD: 'food',
        TEACHER: 'teacher',
        MEDIA: 'media',
        MARKETING: 'marketing',
    }
}